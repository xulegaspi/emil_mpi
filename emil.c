/*
 * emilMPI.c
 *
 *  Created on: Oct 14, 2014
 *      Author: xulegaspi
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#define PI 3.14159265358979323846264


int main (int argc, char *argv[]) {

	// Variable declaration
	double m, ni, mypi, pi, sum, time, total_time, pi_error, error;
	int id, i, ii, jj, kk, iterations, threads, cores1, rep = 10;

	MPI_Init(&argc, &argv);
	//iterations = argv[2];
	/**
	for (kk = 1; kk <= 3; kk++) {
		// Choosing number of iterations
		switch(kk) {
			case 1:
				iterations = 24000000;
				rep = 10;
				break;
			case 2:
				iterations = 48000000;
				rep = 10;
				break;
			case 3:
				iterations = 96000000;
				rep = 5;
				break;
			default:
				break;
		}
		**/

	MPI_Comm_size(MPI_COMM_WORLD, &threads);
	MPI_Comm_rank(MPI_COMM_WORLD, &id);
	cores1 = threads;

	if (id == 0) {
		printf("Enter the number of intervals: (0 quits) ");
		scanf("%d",&iterations);
		printf("\nStart: [Cores: %d | Iterations: %d | Repetitions: %d]\n", cores1, iterations, rep);
	}

	// Initializing values
	time = 0.0, total_time = 0.0, pi_error = 0.0, error = 0.0;

	for (ii = 1; ii < rep; ii++) {

		//time = omp_get_wtime();
		time = MPI_Wtime();

		MPI_Bcast(&iterations, 1, MPI_INT, 0, MPI_COMM_WORLD);

		if(iterations==0) break;
		else {

			m = 1.0 / (double)iterations;
			sum = 0.0;

			// Paralleled coded
			for (i = 0; i < iterations; i++) {
				ni = ((double)i + 0.5) * m;
				sum += 4.0 / (1.0 + ni * ni);
			}


			mypi = m * sum;

			MPI_Reduce(&mypi, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

			//time = omp_get_wtime() - time;
			time = MPI_Wtime() - time;

			total_time += time;
			pi_error = pi_error + (pi - PI);
		}

	}

	error = pi_error/rep;

	if (id == 0) {
		printf(" - Average time: %.30f\n", total_time/rep);
		printf(" -Average error: %.30f\n", error);
		printf("-------------------------------------------------------------------\n");
	}

	MPI_Finalize();
	return 0;
}

